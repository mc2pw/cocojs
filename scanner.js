import * as util from './util';

HEAP_BLOCK_SIZE = 64*1024;

NEWLINE = 10;
CARRAIGE_RETURN = 13;
ASTERISK_CHAR = 42;
SLASH_CHAR = 47;

class TokenRef {
    static GetKind(dv, o) {
        return dv.getInt32(o + TokenRef.KIND);
    }

    static SetKind(dv, o, v) {
        dv.setInt32(o + TokenRef.KIND, v);
    }

    static GetPos(dv, o) {
        return dv.getInt32(o + TokenRef.POS);
    }

    static SetPos(dv, o, v) {
        dv.setInt32(o + TokenRef.POS, v);
    }

    static GetCharPos(dv, o) {
        return dv.getInt32(o + TokenRef.CHAR_POS);
    }

    static SetCharPos(dv, o, v) {
        return dv.setInt32(o + TokenRef.CHAR_POS, v);
    }

    static GetCol(dv, o) {
        return dv.getInt32(o + TokenRef.COL);
    }

    static SetCol(dv, o, v) {
        dv.setInt32(o + TokenRef.COL, v);
    }

    static GetLine(dv, o) {
        return dv.getInt32(o + TokenRef.LINE);
    }

    static SetLine(dv, o, v) {
        dv.setInt32(o + TokenRef.LINE, v);
    }

    static GetNext(dv, o, dst) {
        return util.Ptr.Get(dv, o + TokenRef.NEXT, dst);
    }

    static SetNext(dv, o, v) {
        return util.Ptr.Set(dv, o + TokenRef.NEXT, v);
    }

    static GetVal(dv, o, dst) {
        return util.Ptr.Get(dv, o + TokenRef.VAL, dst);
    }

    static SetVal(dv, o, v) {
        return util.Ptr.Set(dv, o + TokenRef.VAL, v);
    }
}

TokenRef.KIND = 0;
TokenRef.POS = 4;
TokenRef.CHAR_POS = 8;
TokenRef.COL = 12;
TokenRef.LINE = 16;
TokenRef.NEXT = 20;
TokenRef.VAL = 28; // The length is the first two bytes.
TokenRef.SIZE = 36

// TODO: Use a buffer that reads from memory, to avoid problems with async, etc.?
// TODO: The buffer must be read as unicode.
class Buffer {
    constructor(opts) {
        if (opts.s) this._constructor_s(opts.s, opts.isUserStream);
        else if (opts.b) this._constructor_b(opts.b);
        else this._constructor_buf(opts.buf, opts.len);
    }

    _constructor_s = (s, isUserStream) => {
        this.stream = s;
        this.isUserStream = isUserStream;
        if (this.CanSeek()) {
            this.fileLen = util.fsize(s);
            this.bufLen = (fileLen < MAX_BUFFER_LENGTH) ? fileLen : MAX_BUFFER_LENGTH;
            this.bufStart = util.INT_MAX;
        } else {
            this.fileLen = this.bufLen = this.bufStart = 0;
        }
        this.bufCapacity = (bufLen>0) ? bufLen : MIN_BUFFER_LENGTH;
        this.buf = new Uint8Array(this.bufCapacity);
    }

    InitStream = async () => {
        // Call this when creating a Buffer from a stream.
        if (this.fileLen > 0) await this.SetPos(0);
        else this.bufPos = 0;
        if (this.bufLen == this.fileLen && this.CanSeek()) this.Close();
    }

    _constructor_b = (b) => {
        this.buf = b.buf;
        this.bufCapacity = b.bufCapacity;
        b.buf = null;
        this.bufStart = b.bufStart;
        this.bufLen = b.bufLen;
        this.fileLen = b.fileLen;
        this.bufPos = b.bufPos;
        this.stream = b.stream;
        b.stream = null;
        this.isUserStream = b.isUserStream;
    }

    _constructor_buf = (buf, len) => {
        this.buf = new Uint8Array(buf, 0, len);
        this.bufStart = 0;
        this.bufCapacity = this.bufLen = len;
        this.fileLen = len;
        this.bufPos = 0;
        this.stream = null;
    }

    Close = () => {
        if (!this.isUserStream && this.stream) {
            util.fclose(this.stream);
            this.stream = null;
        }
    }

    Read = async () => {
        if (this.bufPos < bufLen) {
            return this.buf[this.bufPos++];
        } else if (this.GetPos() < this.fileLen) {
            await this.SetPos(this.GetPos());
            return this.buf[this.bufPos++];
        } else if (this.stream && !this.CanSeek() && (await this.ReadNextStreamChunk() > 0)) {
            return Buffer.EoF;
        }
    }

    Peek = async () => {
        var curPos = this.GetPos();
        var ch = await this.Read();
        await this.SetPos(curPos);
        return ch;
    }

    GetString = async (beg, end) => {
        var len = 0;
        var buf = new Uint8Array(end - beg);
        var oldPos = this.GetPos();
        await this.SetPos(beg);
        while (this.GetPos() < end) buf[len++] = this.Read();
        await this.SetPos(oldPos);
        util.assert(buf.length === len);
        return new TextDecoder("utf-8").decode(buf);
    }

    GetPos = () => {
        return this.bufPos + this.bufStart;
    }

    SetPos = async (value) => {
        if((value >= this.fileLen) && this.stream && !this.CanSeek()) {
            while((value >= this.fileLen) && (await this.ReadNextStreamChunk() > 0));
        }

        if ((value < 0) || (value > this.fileLen)) {
            util.print(`--- buffer out of bounds access, position: ${value}`);
            util.exit(1);
        }

        if ((value >= this.bufStart) && (value < (this.bufSart + this.bufLen))) {
            this.bufPos = value - this.bufStart;
        } else if (this.stream) {
            // Read starting at position value of the stream.
            // Copy to buf starting at position 0.
            util.fseek(this.stream, value);
            this.bufLen = await util.fread(this.buf, 0, this.bufCapacity, this.stream);
            this.bufStart = value; this.bufPos = 0;
        } else {
            this.bufPos = this.fileLen - this.bufStart;
        }
    }

    ReadNextStreamChunk = async () => {
        const free = this.bufCapacity - this.bufLen;
        if (free === 0) {
            this.bufCapacity = this.bufLen * 2;
            const newBuf = new Uint8Array(this.buf, 0, this.bufCapacity);
            this.buf = newBuf;
            free = this.bufLen;
        }
        const read = util.read(this.buf, bufLen, free, this.stream);
        if (read > 0) {
            this.fileLen = this.bufLen = (this.bufLen + read);
            return read;
        }
        // end of stream reached
        return 0;
    }

    CanSeek = () => {
        return stream && util.ftell(stream) != -1;
    }
}

Buffer.EoF = WCHAR_MAX + 1;


class Scanner {
    constructor(opts) {
        if (opts.buf) this._constructor_buf(opts.buf, opts.len);
        else if (opts.fileName) this._constructor_fileName(opts.fileName);
        else this._constructor_s(opts.s);
    }

    _constructor_buf = (buf, len) => {
        this.buffer = new Buffer({ buf, len });
    }

    _constructor_fileName = (fileName) => {
        this.chFileName = fileName;
    }

    _constructor_s = (s) => {
        const isUserStream = true;
        this.buffer = new Buffer({ s, isUserStream });
    }

    Init = async () => {
        if (this.chFileName) {
            const stream = await fopenrb(this.chFileName)
            if (!stream) {
                util.print(`--- Cannot open file ${this.chFileName}`);
                util.exit(1);
            }
            const s = stream;
            const isUserStream = false;
            this.buffer = new Buffer({ s, isUserStream });
        }

        if (this.buffer) await this.buffer.InitStream();

        this.InitMaps();
        this.tval = [];

        this.heap = util.Ptr.Alloc(HEAP_BLOCK_SIZE + util.Ptr.SIZE);
        this.firstHeap = this.heap.Offset(0);
        this.heapEnd = this.heap.Offset(HEAP_BLOCK_SIZE);
        util.Ptr.Set(this.heapEnd.Dv(), this.heapEnd.pos, util.Ptr.NULL);
        this.heapTop = this.heap.Offset(0);
        if (TokenRef.SIZE > HEAP_BLOCK_SIZE) {
            util.print("--- Too small HEAP_BLOCK_SIZE");
            util.exit();
        }

        this.pos = -1; this.line = 1; this.col = 0; this.charPos = -1;
        this.oldEols = 0;

        await this.NextCh();
        this.tokens = this.CreateToken(); // first token is a dummy
        this.pt = this.tokens.Offset(0);
    }

    InitMaps = () => {
        const start = this.start = {};
        const keywords = this.keywords = {};
        this.EOL = util.EOL; // \n
        this.eofSym = 0;
        this.maxT = 41;
        this.noSym = 41;
        var i;
        for (i = 65; i <= 90; ++i) start[i] = 1;
        for (i = 95; i <= 95; ++i) start[i] = 1;
        for (i = 97; i <= 122; ++i) start[i] = 1;
        for (i = 48; i <= 57; ++i) start[i] = 2;
        start[34] = 12;
        start[39] = 5;
        start[36] = 13;
        start[61] = 16;
        start[46] = 31;
        start[43] = 17;
        start[45] = 18;
        start[60] = 32;
        start[62] = 20;
        start[124] = 23;
        start[40] = 33;
        start[41] = 24;
        start[91] = 25;
        start[93] = 26;
        start[123] = 27;
        start[125] = 28;
        start[Buffer.EoF] = -1;
        keywords.COMPILER = 6;
        keywords.IGNORECASE = 7;
        keywords.CHARACTERS = 8;
        keywords.TOKENS = 9;
        keywords.PRAGMAS = 10;
        keywords.COMMENTS = 11;
        keywords.FROM = 12;
        keywords.TO = 13;
        keywords.NESTED = 14;
        keywords.IGNORE = 15;
        keywords.PRODUCTIONS = 16;
        keywords.END = 19;
        keywords.ANY = 23;
        keywords.WEAK = 29;
        keywords.SYNC = 26;
        keywords.IF = 37;
        keywords.CONTEXT = 38;
    }

    NextCh = async () => {
        if (this.oldEols > 0) { this.ch = this.EOL; this.oldEols--; }
        else {
            this.pos = this.buffer.GetPos();
            this.ch = await this.buffer.Read(); this.col++; this.charPos++;
            if (ch === util.CARRIAGE_RETURN && this.buffer.Peek() !== util.NEWLINE) this.ch = this.EOL;
            if (this.ch === this.EOL) { this.line++; this.col = 0; }
        }
    }

    AddCh = async () => {
        if (this.ch != Buffer.EoF) {
            this.tval.push(this.ch);
            this.tlen++;
            await this.NextCh();
        }
    }

    Comment0 = async () => {
        var level = 1, pos0 = this.pos, line0 = this.line, col0 = this.col, chatPos0 = this.charPos;
        await NextCh();
        if (this.ch === SLASH_CHAR) { // SLASH_CHAR is '/'
            await this.NextCh();
            for(;;) {
                if (this.ch === 10) {
                    level--;
                    if (level == 0) { this.oldEols = this.line - line0; await this.NextCh(); return true; }
                    await this.NextCh();
                } else if (this.ch == this.buffer.EoF) return false;
                else await NextCh();
            }
        } else {
            await this.buffer.SetPos(pos0); await this.NextCh(); this.line = line0; this.col = col0; this.charPos = charPos0;
        }
        return false;
    }

    Comment1 = async () => {
        var level = 1, pos0 = this.pos; line0 = this.line, col0 = this.col, charPos0 = this.charPos;
        await this.NextCh();
        if (this.ch === ASTERISK_CHAR) {
            await this.NextCh();
            for(;;) {
                if (this.ch === ASTERISK_CHAR) {
                    await this.NextCh();
                    if (this.ch === SLASH_CHAR) {
                        level--;
                        if (level === 0) { this.oldEols = this.line - line0; await this.NextCh(); return true; }
                        await this.NextCh();
                    }
                } else if (this.ch === SLASH_CHAR) {
                    await this.NextCh();
                    if (this.ch === ASTERISK_CHAR) {
                        level++; await this.NextCh();
                    }
                } else if (this.ch === this.buffer.EoF) return false;
                else await this.NextCh();
            }
        } else {
            await this.buffer.SetPos(pos0); await this.NextCh(); this.line = line0; this.col = col0; this.charPos = charPos0;
        }
        return false;
    }

    CreateHeapBlock = () => {
        var newHeap;
        var cur = this.firstHeap.Offset(0);

        while (this.tokens.Cmp(cur, 0) < 0 || this.tokens.Cmp(cur, HEAP_BLOCK_SIZE) > 0) {
            util.Ptr.Get(cur.Dv(), cur.pos + HEAP_BLOCK_SIZE, cur);
            this.firstHeap.Free();
            cur.CopyTo(this.firstHeap, 0);
        }

        newHeap = util.Ptr.Alloc(HEAP_BLOCK_SIZE + util.Ptr.SIZE);
        util.Ptr.Set(this.heapEnd.Dv(), this.heapEnd.pos, newHeap);
        this.heapEnd = newHeap.Offset(HEAP_BLOCK_SIZE);
        util.Ptr.Set(this.heapEnd.Dv(), this.heapEnd.pos, 0);
        this.heap = newHeap;
        this.heap.CopyTo(this.heapTop, 0);
    }

    CreateToken = (_t) => {
        var t = _t;
        if (this.heapEnd.Cmp(this.heapTop, TokenRef.SIZE) <= 0) {
            this.CreateHeapBlock();
        }
        t = this.heapTop.CopyTo(t, 0);
        this.heapTop.CopyTo(this.heapTop, TokenRef.SIZE);
        TokenRef.SetVal(t.Dv(), t.pos, util.Ptr.NULL);
        TokenRef.SetNext(t.Dv(), t.pos, util.Ptr.NULL);
        return t;
    }

    AppendVal = (t) => {
        util.assert(this.tval.length === this.tlen);
        const reqMem = 4 + this.tval.length * util.WCHAR_SIZE; // 4 bytes for the length
        if (this.heapEnd.Cmp(this.heapTop, reqMem) <= 0) {
            if (reqMem > HEAP_BLOCK_SIZE) {
                util.print("--- Too long token value");
                util.exit(1);
            }
            this.CreateHeapBlock();
        }
        const tval = this.heapTop;
        TokenRef.SetVal(t.Dv(), t.pos, tval);
        this.heapTop.CopyTo(this.heapTop, reqMem);

        util.UnicodeCopy(tval, this.tval);
    }

    NextToken = (_t) => {
        const ch = this.ch;
        while (
            ch === 32 || // ' ' = 32
            (ch >= 9 && ch <= 10) || ch == 13
        ) this.NextCh();
        if ((ch == SLASH_CHAR && this.Comment0()) || (ch == SLASH_CHAR && this.Comment1())) return this.NextToken();
        var recKind = this.noSym;
        var recEnd = this.pos;
        const t = this.CreateToken(_t);
        const tDv = t.Dv();
        const tpos = t.pos;
        TokenRef.SetPos(tDv, tpos, this.pos);
        TokenRef.SetCol(tDv, tpos, this.col);
        TokenRef.SetLine(tDv, tpos, this.line);
        TokenRef.SetCharPos(tDv, tpos, this.charPos);
        const state = this.start[ch];
        this.tlen = 0; this.AddCh();

        var s = state;
        loop:
        while (1) {
            switch (s) {
                case -1:
                    TokenRef.SetKind(tDv, tpos, eofSym);
                    break loop; // NetCh already done
                case 0:
                    if (recKind !== this.noSym) {
                        this.tlen = recEnd - TokenRef.GetPos(tDv, tpos);
                        this.SetScannerBehindT();
                    }
                    TokenRef.SetKind(tDv, tpos, recKind); break loop;
                    // NextCh already done
                case 1:
                    // '0' = 48, '9' = 57, 'A' = 65, 'Z' = 90, '_' = 95, 'a' = 97, 'z' = 122
                    if ((ch >= 48 && ch <= 57) || (ch >= 65 && ch <= 90) || ch == 95 || (ch >= 97 && ch <= 122)) {this.AddCh(); s = 1; break;}
                    else {
                        TokenRef.SetKind(tDv, tpos, 1);
                        var literal = String.fromCharCode.apply(null, this.tval);
                        if (keywords[literal] !== undefined) TokenRef.SetKind(tDv, tpos, keywords[literal]);
                        break loop;
                    }
                case 2:
                    recEnd = this.pos; recKind = 2;
                    if ((ch >= 48 && ch <= 57)) {AddCh(); s = 2; break;}
                    else {TokenRef.SetKind(tDv, tpos, 2); break loop;}
                case 3:
                    TokenRef.SetKind(tDv, tpos, 3); break loop;
                case 4:
                    TokenRef.SetKind(tDv, tpos, 4); break loop;
                case 5:
                    // '&' = 38, '(' = 40, '[' = 91, ']' = 93
                    if (ch <= 9 || (ch >= 11 && ch <= 12) || (ch >= 14 && ch <= 38) || (ch >= 40 && ch <= 91) || (ch >= 93 && ch <= 65535)) {this.AddCh(); s = 6; break;}
                    else if (ch == 92) {AddCh(); s = 7; break;}
                    else {s = 0; break;}
                case 6:
                    if (ch === 39) {this.AddCh(); s = 9; break;}
                    else {s = 0; break;}
                case 7:
                    // ' ' = 32, '~' = 126
                    if (ch >= 32 && ch <= 126) {this.AddCh(); s = 8; break;}
                    else {s = 0; break;}
                case 8:
                    // '0' = 48, '9' = 57, 'a' = 97, 'f' = 102
                    if ((ch >= 48 && ch <= 57) || (ch >= 97 && ch <= 102)) {this.AddCh(); s = 8; break;}
                    else if (ch === 39) {this.AddCh(); s = 9; break;}
                    else {s = 0; break;}
                case 9:
                    TokenRef.SetKind(tDv, tpos, 5); break loop;
                case 10:
                    recEnd = this.pos; recKind = 42;
                    // '0' = 48, '9' = 57, 'A' = 65, 'Z' = 90, '_' = 95, 'a' = 97, 'z' = 122
                    if ((ch >= 48 && ch <= 57) || (ch >= 65 && ch <= 90) || ch === 95 || (ch >= 97 && ch <= 122)) {this.AddCh(); s = 10; break;}
                    else {TokenRef.SetKind(tDv, tpos, 42); break loop;}
                case 11:
                    recEnd = this.pos; this.recKind = 43;
                    // '-' = 45, '.' = 46, '0' = 48, ':' = 58, 'A' = 65, 'Z' = 90, '_' = 95, 'a' = 97, 'z' = 122
                    if ((ch >= 45 && ch <= 46) || (ch >= 48 && ch <= 58) || (ch >= 65 && ch <= 90) || ch === 95 || (ch >= 97 && ch <= 122)) {this.AddCh(); s = 11; break;}
                    else {TokenRef.SetKind(tDv, tpos, 43); break loop;}
                case 12:
                    // '!' = 33, '#' = 35, '[' = 91, ']' = 93
                    if (ch <= 9 || (ch >= 11 && ch <= 12) || (ch >= 14 && ch <= 33) || (ch >= 35 && ch <= 91) || (ch >= 93 && ch <= 65535)) {this.AddCh(); s = 12; break;}
                    else if (ch === 10 || ch === 13) {this.AddCh(); s = 4; break;}
                    else if (ch === 34) {this.AddCh(); s = 3; break;} // '"' = 34
                    else if (ch === 92) {this.AddCh(); s = 14; break;}
                    else {s = 0; break;}
                case 13:
                    recEnd = this.pos; recKind = 42;
                    // '0' = 48, '9' = 57, 'A' = 65, 'Z' = 90, '_' = 95, 'a' = 97, 'z' = 122
                    if (ch >= 48 && ch <= 57) {this.AddCh(); s = 10; break;}
                    else if ((ch >= 65 && ch <= 90) || ch === 95 || (ch >= 97 && ch <= 122)) {this.AddCh(); s = 15; break;}
                    else {TokenRef.SetKind(tDv, tpos, 42); break loop;}
                case 14:
                    if (ch >= 32 && ch <= 126) {this.AddCh(); s = 12; break;}
                    else {s = 0; break;}
                case 15:
                    recEnd = this.pos; recKind = 42;
                    if ((ch >= 48 && ch <= 57)) {this.AddCh(); s = 10; break;}
                    else if ((ch >= 65 && ch <= 90) || ch === 95 || (ch >= 97 && ch <= 122)) {this.AddCh(); s = 15; break;}
                    else if (ch === 61) {this.AddCh(); s = 11; break;} // '=' = 61
                    else {TokenRef.SetKind(tDv, tpos, 42); break loop;}
                case 16:
                    TokenRef.SetKind(tDv, tpos, 17); break loop;
                case 17:
                    TokenRef.SetKind(tDv, tpos, 20); break loop;
                case 18:
                    TokenRef.SetKind(tDv, tpos, 21); break loop;
                case 19:
                    TokenRef.SetKind(tDv, tpos, 22); break loop;
                case 20:
                    TokenRef.SetKind(tDv, tpos, 25); break loop;
                case 21:
                    TokenRef.SetKind(tDv, tpos, 26); break loop;
                case 22:
                    TokenRef.SetKind(tDv, tpos, 27); break loop;
                case 23:
                    TokenRef.SetKind(tDv, tpos, 28); break loop;
                case 24:
                    TokenRef.SetKind(tDv, tpos, 31); break loop;
                case 25:
                    TokenRef.SetKind(tDv, tpos, 32); break loop;
                case 26:
                    TokenRef.SetKind(tDv, tpos, 33); break loop;
                case 27:
                    TokenRef.SetKind(tDv, tpos, 34); break loop;
                case 28:
                    TokenRef.SetKind(tDv, tpos, 35); break loop;
                case 29:
                    TokenRef.SetKind(tDv, tpos, 39); break loop;
                case 30:
                    TokenRef.SetKind(tDv, tpos, 40); break loop;
                case 31:
                    recEnd = this.pos; recKind = 18;
                    if (ch == 46) {this.AddCh(); s = 19; break;} // '.' = 46
                    else if (ch == 62) {this.AddCh(); s = 22; break;} // '>' = 62
                    else if (ch == 41) {this.AddCh(); s = 30; break;} // ')' = 41
                    else {TokenRef.SetKind(tDv, tpos, 18); break loop;}
                case 32:
                    recEnd = this.pos; recKind = 24;
                    if (ch == 46) {this.AddCh(); s = 21; break;} // '.' = 46
                    else {TokenRef.SetKind(tDv, tpos, 24); break loop;}
                case 33:
                    recEnd = this.pos; recKind = 30;
                    if (ch == 46) {this.AddCh(); s = 29; break;} // '.' = 46
                    else {TokenRef.SetKind(tDv, tpos, 30); break loop;}

            }
        }
        this.AppendVal(t);
        return t;
    }

    SetScannerBehindT = () => {
        const tDv = this.t.Dv();
        const tpos = this.t.pos;
        this.buffer.SetPos(TokenRef.GetPos(tDv, tpos));
        this.NextCh();
        this.line = TokenRef.GetLine(tDv, tpos);
        this.col = TokenRef.GetCol(tDv, tpos);
        this.charPos = TokenRef.GetCharPos(tDv, tpos);
        util.assert(this.tlen === this.tval.length);
        for (var i = 0; i < this.tlen; i++) this.NextCh();
    }

    // TODO: needs to be async, or at least return a promise in certain cases
    // so as to avoid having to allocate memory every time.
    Scan = (_t) => {
        var t = _t;
        this.tokens = TokenRef.GetNext(this.tokens.Dv(), this.tokens.pos, this.tokens);
        if (this.tokens.Cmp(util.Ptr.NULL, 0) === 0) {
            this.tokens = this.NextToken(this.tokens);
        }
        this.tokens.CopyTo(this.pt, 0);
        t = this.pt.CopyTo(t, 0);
        return t;
    }

    Peek = (_t) => {
        var tnext = _t;
        do {
            tnext = TokenRef.GetNext(this.pt.Dv(), this.pt.pos, tnext);
            if (tnext.Cmp(util.Ptr.NULL, 0) === 0) {
                tnext = this.NextToken(tnext);
                TokenRef.SetNext(this.pt.Dv(), this.pt.pos, tnext);
            }
            tnext.CopyTo(this.pt, 0);
        } while (TokenRef.GetKind(this.pt.Dv(), this.pt.pos) > this.MaxT);

        return tnext;
    }

    ResetPeek = () => {
        this.tokens.CopyTo(this.pt, 0);
    }
}

export TokenRef;
