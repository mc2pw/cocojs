WCHAR_SIZE = 2;

class Ptr {
    constructor(key, pos) {
        this.key = key;
        this.pos = pos;
    }

    static Alloc(size) {
        const buf = new Uint8Array(size).buffer;
        Ptr.mem.set(Ptr.counter, new DataView(buf));
        // We assume sure the buffer is aligned with 32 bits.
        Ptr.memArr16.set(Ptr.counter, new Uint16Array(arr));
        Ptr.counter++;
        return new Ptr(Ptr.counter, 0);
    }

    Dv = () => {
        return Ptr.mem.get(this.key);
    }

    Offset = (offset) => {
        return new Ptr(this.key, this.pos + offset);
    }

    Free = () => {
        Ptr.mem.delete(this.key);
        Ptr.memArr16.delete(this.key);
    }

    Cmp = (v, offset) => {
        if (this.key > v.key) return 1;
        if (this.key < v.key) return -1;
        var pos = v.pos + offset;
        if (this.pos > pos) return 1;
        if (this.pos < pos) return -1;
        return 0;
    }

    static Get(dv, offset, dst) {
        dst = dst || new Ptr();
        dst.key = dv.getUint32(offset)
        dst.pos = dv.getUint32(offset + 4);
        return dst;
    }

    static Set(dv, offset, v) {
        dv.setUint32(offset, v.key);
        dv.setUint32(offset+4, v.pos);
    }

    CopyTo = (v, offset) => {
        v = v || new Ptr();
        v.key = this.key;
        v.pos = this.pos + offset;
        return v;
    }
}

Ptr.mem = new Map();
Ptr.memArr16 = new Map();
Ptr.counter = 1;
Ptr.SIZE = 8;
Ptr.NULL = new Ptr(0, 0);

function UnicodeCopy(ptr, codes) {
    const dv = ptr.Dv();
    const pos = ptr.pos;
    const arr = Ptr.memArr16.get(ptr.key);
    dv.setInt32(pos, codes.length);
    assert(pos % 2 === 0);
    arr.set(codes, pos/2 + 2);
}

function UnicodeLength(ptr) {
    return ptr.Dv().getInt32(ptr.pos);
}

function UnicodeLoad(ptr) {
    const dv = ptr.Dv();
    const pos = ptr.pos;
    const length = dv.getInt32(pos);
    return String.fromCharCode.apply(null, new Uint16Array(dv.buffer, pos+4, length));
}
