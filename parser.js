
import * as util from './util';
import { TokenRef } as scanner from './scanner';

class Parser {
    SynErr = (n) => {
        if (this.errDist >= this.minErrDist) {
            const line = TokenRef.GetLine(this.laDv, this.lapos);
            const col = TokenRef.GetCol(this.laDv, this.lapos);
            this.errors.SynErr(line, col, n);
        }
        this.errDist = 0;
    }

    SemErr = (msg) => {
        if (this.errDist >= this.minErrDist) {
            const line = TokenRef.GetLine(this.tDv, this.tpos);
            const col = TokenRef.GetCol(this.tDv, this.tpos);
            this.errors.Error(line, col, n);
        }
        this.errDist = 0;
    }

    Get = () => {
        for (;;) {
            this.la.CopyTo(this.t, 0);
            this.scanner.Scan(this.la);
            const laDv = this.la.Dv();
            const lapos = this.la.pos;
            const laKind = TokenRef.GetKind(laDv, lapos);
            if (laKind <= this.maxT) { ++this.errDist; break; }
            if (laKind === Parser._ddtSym) {
                this.tab.SetDDT(TokenRef.GetVal(laDv, lapos));
            }
            if (laKind === Parser._optionSym ) {
                this.tab.SetOption(TokenRef.GetVal(laDv, lapos));
            }

            if (this.dummyToken.Cmp(this.t) !== 0) {
                const tDv = this.t.Dv();
                const tpos = this.t.pos;
                const dDv = this.t.Dv();
                const dpos = this.t.pos;
                TokenRef.SetKind(dDv, dpos, TokenRef.GetKind(tDv, tpos));
                TokenRef.SetPos(dDv, dpos, TokenRef.GetPos(tDv, tpos));
                TokenRef.SetCol(dDv, dpos, TokenRef.GetCol(tDv, tpos));
                TokenRef.SetLine(dDv, dpos, TokenRef.GetLine(tDv, tpos));
                TokenRef.SetNext(dDv, dpos, TokenRef.GetNext(tDv, tpos));
                TokenRef.SetVal(dDv, dpos, TokenRef.GetVal(tDv, tpos));
                this.dummyToken.CopyTo(this.t, 0);
            }
            this.t.CopyTo(this.la);
        }
        this._la_t_changed();
    }

    Expect = (n) => {
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === n) this.Get();
        else this.SynErr(n);
    }

    ExpectWeak = (n, follow) => {
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === n) this.Get();
        else {
            this.SynErr(n);
            while (!this.StartOf(follow)) this.Get();
        }
    }

    WeakSeparator = (n, syFol, repFol) => {
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === n) {this.Get(); return true;}
        else if (thisStartOf(repFol)) return false;
        else {
            this.SynErr(n);
            while (!(this.StartOf(syFol) || this.StartOf(repFol) || this.StartOf(0))) {
                this.Get();
            }
            return this.StartOf(syFol);
        }
    }

    _la_t_changed = () => {
        this.laDv = this.la.Dv(); this.lapos = this.la.pos;
        this.tDv = this.t.Dv(); this.tpos = this.t.pos;
    }

    Coco = () => {
        var sym, gramName;
        const gR = [null];
        const g1R = [null];
        const g2R = [null]; // Used as pass-by-ref args.
        const sR = [null];

        // TODO: Check for changing tDv, tpos everywhere else!
        var beg = TokenRef.GetPos(this.laDv, this.lapos);
        var line = TokenRef.GetLine(this.laDv, this.lapos);
        var laKind;
        while (this.StartOf(1)) {
            this.Get();
        }
        const tVal = TokenRef.GetVal(this.tDv, this.tpos);
        if (TokenRef.GetPos(this.laDv, this.lapos) !== beg) {
            this.pgen.usingPos = new Position(beg, TokenRef.GetPos(this.tDv, this.tpos) + util.UnicodeLength(tVal), 0, line);
        }

        this.Expect(6 /* "COMPILER" */);
        this.genScanner = true;
        this.tab.ignored = new CharSet();
        this.Expect(Parser._indent);
        gramName = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos, tVal));
        beg = TokenRef.GetPos(this.laDv, this.lapos);
        line = TokenRef.GetLine(this.laDv, this.lapos);

        while (this.StartOf(2)) {
            this.Get();
        }
        this.tab.semDeclPos = new Position(beg, TokenRef.GetPos(this.laDv, this.lapos), 0, line);
        if (TokenRef.GetKind(this.laDv, this.lapos) === 7 /* "IGNORECASE" */) {
            this.Get();
            this.dfa.ignoreCase = true;
        }
        if (TokenRef.GetKind(this.laDv, this.lapos) === 8 /* "CHARACTERS" */) {
            this.Get();
            while (TokenRef.GetKind(this.laDv, this.lapos) === Parser._ident) {
                this.SetDecl(tVal);
            }
        }
        if (TokenRef.GetKind(this.laDv, this.lapos) === 9 /* "TOKENS" */) {
            this.Get();
            laKind = TokenRef.GetKind(this.laDv, this.lapos);
            while (laKind === Parser._ident || laKind === Parser._string || laKind === Parser._char) {
                this.TokenDecl(Node.t);
                laKind = TokenRef.GetKind(this.laDv, this.lapos)
            }
        }
        if (TokenRef.GetKind(this.laDv, this.lapos) === 10 /* "PRAGMAS" */) {
            this.Get();
            laKind = TokenRef.GetKind(this.laDv, this.lapos);
            while (laKind === Parser._ident || laKind === Parser._string || laKind === Parser._char) {
                this.TokenDecl(Node.pr);
                laKind = TokenRef.GetKind(this.laDv, this.lapos)
            }
        }
        while (TokenRef.GetKind(this.laDv, this.lapos) === 11 /* "COMMENTS" */) {
            this.Get();
            var nested = false;
            this.Expect(12 /* "FROM" */);
            this.TokenExpr(g1R);
            this.Expect(13 /* "TO" */);
            this.TokenExpr(g2R);
            if (TokenRef.GetKind(this.laDv, this.lapos) === 14 /* "NESTED" */) {
                this.Get();
                nested = true;
            }
            this.dfa.NewComment(g1R[0].l, g2R[0].l, nested);
        }
        while (TokenRef.GetKind(this.laDv, this.lapos) === 15 /* "IGNORE" */) {
            this.Get();
            this.Set(sR);
            this.tab.ignored.Or(sR[0]);
        }
        laKind = TokenRef.GetKind(this.laDv, this.lapos);
        while (!(laKind === Parser._EOF || laKind === 12 /* "PRODUCTIONS" */)) {
            this.SynErr(42); this.Get();
            laKind = TokenRef.GetKind(this.laDv, this.lapos);
        }
        this.Expect(16 /* "PRODUCTIONS" */);
        if (this.genScanner) this.dfa.MakeDeterministic();
        this.tab.DeleteNodes();

        while(TokenRef.GetKind(this.laDv, this.lapos) === Parser._ident) {
            this.Get();
            sym = this.tab.FindSym(TokenRef.GetVal(this.tDv, this.tpos, tVal));
            var undef = !sym;
            var tLine = TokenRef.GetLine(this.tDv, this.tpos);
            if (undef) sym = tab.NewSym(Node.nt, tVal, tLine);
            else {
                if (sym.type === Node.nt) {
                    if (!sym.graph) this.SemErr("name declared twice");
                } else this.SemErr("this symbol kind not allowed on left of production");
                sym.line = tLine;
            }
            var noAttrs = !sym.attrPos;
            sym.attrPos = null;

            laKind = TokenRef.GetKind(this.laDv, this.lapos);
            if (laKind === 24 /* "<" */ || laKind === 26 /* "<." */) {
                this.AttrDecl(sym);
            }
            if (!undef)
                if (noAttrs !== !sym.attrPos)
                    this.SemErr("attribute mismatch between declaration and use of this symbol");

            laKind = TokenRef.GetKind(this.laDv, this.lapos);
            if (laKind === 39 /* "(." */) {
                this.SemText(sym); // Sets sym.sempos
            }
            this.ExpectWeak(17 /* "=" */, 3);
            this.Expression(gR);
            sym.graph = gR[0].l;
            tab.Finish(gR[0]);

            this.ExpectWeak(18 /* "." */, 4);
        }
        this.Expect(19 /* "END" */);
        this.Expect(Parser._ident);
        if (gramName === util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos, tVal))
            this.SemErr("name does not match grammar name");
        this.tab.gramSy = this.tab.FindSym(gramName);
        if (!this.tab.gramSy)
            this.SemErr("missing production for grammar name");
        else {
            sym = this.tab.gramSy;
            if (sym.attrPos)
                this.SemErr("grammar symbol must not have attributes");
        }
        this.tab.noSym = this.tab.NewSym(Node.t, "???", 0); // noSym gets nighest number
        this.tab.SetupAnys();
        this.tab.RenumberPragmas();
        if (this.tab.ddt[2]) this.tab.PrintNodes();
        if (this.errors.count === 0) {
            util.Print("checking");
            this.tab.CompSymbolSets();
            if (this.tab.ddt[7]) this.tab.XRef();
            if (this.tab.GrammarOk()) {
                util.Print("parser");
                this.pgen.WriteParser();
                if (this.genScanner) {
                    // TODO: Fix Print calls that don't have tacit \n at the end!!
                    util.Print(" + scanner");
                    this.dfa.WriteScanner();
                    if (this.tab.ddt[0]) this.dfa.PrintStates();
                }
                util.Print(" generated\n");
                if (this.tab.ddt[8]) this.pgen.WriteStatistics();
            }
        }
        if (this.tab.ddt[6]) this.tab.PrintSymbolTables();

        this.Expect(18 /* "." */);
    }

    SetDecl = (_tVal, _sR) => {
        const sR = _sR || [null];
        this.Expect(Parser._ident);
        const name = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos, _tVal));
        const c = this.tab.FindCharClass(name);
        if (c) this.SemErr("name declared twice");
        this.Expect(17 /* "=" */);
        this.Set(sR);
        if (sR[0].Elements() === 0) this.SemErr("character set must not be empty");
        this.tab.NewCharClass(name, sR[0]);
        this.Expect(18 /* "." */);
    }

    TokenDecl = (typ, _kindR, _nameR, _gR) => {
        var sym;
        const kindR = _kindR || [null];
        const nameR = _nameR || [null];
        const gR = _gR || [null];
        this.Sym(nameR, kindR);
        sym = this.tab.FindSym(name);
        if (sym) this.SemErr("name declared twice");
        else {
            sym = this.tab.NewSym(typ, nameR[0], TokenRef.GetLine(this.tDv, this.tpos));
            sym.tokenKind = Symbol.fixedToken;
        }
        this.tokenString = null;

        while (!this.StartOf(5)) {this.SynErr(43); this.Get();}
        if (TokenRef.GetKind(this.laDv, this.lapos) === 17 /* "=" */) {
            this.Get();
            this.TokenExpr(gR);
            this.Expect(18 /* "." */);
            if (kindR[0] === this.str) this.SemErr("a literal must not be declared with a structure");
            this.tab.Finish(gR[0]);
            if (this.tokenString === null || this.tokenString === this.noString)
                this.dfa.ConvertToStates(gR[0].l, sym);
            else { // TokenExpr is a single string
                if (this.tab.literals[tokenString])
                    this.SemErr("token string declared twice");
                this.tab.literals[this.tokenString] = sym;
                this.dfa.MatchLiteral(this.tokenString, sym);
            }

        } else if (this.StartOf(6)) {
            if (kindR[0] === this.id) this.genScanner = false;
            else this.dfa.MatchLiteral(sym.name, sym);
        } else this.SynErr(44);
        if (TokenRef.GetKind(this.laDv, this.lapos) === 39 /* "(." */) {
            this.SemText(sym); // Sets sym.sempos
            if (typ != Node.pr) this.SemErr("semantic action not allowed here");
        }
    }

    TokenExpr = (gR, _g2R) => {
        const g2R = _g2R || [null];
        this.TokenTerm(gR);
        var first = true;
        while (this.WeakSeparator(28 /* "|" */, 8, 7)) {
            this.TokenTerm(g2R);
            if (first) { this.tab.MakeFirstAlt(gR[0]); first = false; }
            this.tab.MakeAlternative(gR, gR2);
        }
    }

    Set = (sR, _s2R) => {
        const s2R = _s2R || [null];
        this.SimSet(sR);
        var laKind = TokenRef.GetKind(this.laDv, this.lapos);
        while (laKind === 20 /* "+" */ || laKind === 21 /* "-" */) {
            if (laKind === 20 /* "+" */) {
                this.Get();
                this.SimSet(s2R);
                sR[0].Or(s2R[0]);
            } else {
                this.Get();
                this.SimSet(s2R);
                sR[0].Subtract(s2R[0]);
            }
            laKind = TokenRef.GetKind(this.laDv, this.lapos);
        }
    }

    AttrDecl = (sym) => {
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === 24 /* "<" */) {
            this.Get();
            const beg = TokenRef.GetPos(this.laDv, this.lapos);
            const col = TokenRef.GetCol(this.laDv, this.lapos);
            const line = TokenRef.GetLine(this.laDv, this.lapos);
            while (this.StartOf(9)) {
                if (this.StartOf(10)) {
                    this.Get();
                } else {
                    this.Get();
                    this.SemErr("bad string in attributes");
                }
            }
            this.Expect(25 /* ">" */);
            const tPos = TokenRef.GetPos(this.tDv, this.tpos);
            if (tPos > beg)
                sym.attrPos = new Position(beg, tPos, col, line);
        } else if (laKind === 26 /* "<." */) {
            this.Get();
            const beg = TokenRef.GetPos(this.laDv, this.lapos);
            const col = TokenRef.GetCol(this.laDv, this.lapos);
            const line = TokenRef.GetLine(this.laDv, this.lapos);
            while (this.StartOf(11)) {
                if (this.StartOf(12)) {
                    this.Get();
                } else {
                    this.Get();
                    this.SemErr("bad string in attributes");
                }
            }
            this.Expect(27 /* ".>" */);
            const tPos = TokenRef.GetPos(this.tDv, this.tpos);
            if (tPos > beg)
                sym.attrPos = new Position(beg, tPos, col, line);
        } else this.SynErr(45);
    }

    SemText = (sym) => {
        this.Expect(39 /* "(." */);
        const beg = TokenRef.GetPos(this.laDv, this.lapos);
        const col = TokenRef.GetCol(this.laDv, this.lapos);
        const line = TokenRef.GetLine(this.tDv, this.tpos);
        while (this.StartOf(13)) {
            if (this.StartOf(14)) {
                this.Get();
            } else if (TokenRef.GetKind(this.laDv, this.lapos) === Parser._badString) {
                this.Get();
                this.SemErr("bad string in semantic action");
            } else {
                this.Get();
                this.SemErr("missing end of previous semantic action");
            }
        }
        this.Expect(40 /* ".)" */);
        const tPos = TokenRef.GetPos(this.tDv, this.tpos);
        sym.pos = new Position(beg, tPos, col, line);
    }

    Expression = (gR, _g2R) => {
        const g2R = _g2R || [null];
        this.Term(gR);
        var first = true;
        while (this.WeakSeparator(28 /* "|" */, 16, 15)) {
            this.Term(g2R);
            if (first) { this.tab.MakeFirstAlt(gR[0]); first = false; }
            this.tab.MakeAlternative(gR[0], g2R[0]);
        }
    }

    SimSet = (sR, _n1R, _n2R) => {
        const n1R = _n1R || [null];
        const n2R = _n2R || [null];
        var s = new CharSet(); sR[0] = s;
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === Parser._ident) {
            this.Get();
            // TODO: Check that a string is passed instead of a Ptr in other places too.
            const c = tab.FindCharClass(util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos)));
            if (c === null) this.SemErr("undefined name"); else s.Or(c.set);
        } else if (laKind === Parser._string) {
            this.Get();
            const tValU = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos));
            const subName2 = tValU.substr(1, tValU.length-2);
            const name = this.tab.Unescape(subName2);
            var ch;
            var len = name.length;
            if (this.dfa.ignoreCase) {
                name = name.toLowerCase();
            }
            for (var i = 0; i < len; i++) {
                ch = name[i].charCodeAt();
                s.Set(ch);
            }
        } else if (laKind === Parser._char) {
            this.Char(n1R);
            s.Set(n1R[0]);
            if (TokenRef.GetKind(this.laDv, this.lapos) === 22 /* ".." */) {
                this.Get();
                this.Char(n2R);
                const n1 = n1R[0];
                const n2 = n2R[0];
                for (var i = n1; i <= n2; i++) s.Set(i);
            }
        } else if (laKind === 23 /* "ANY" */) {
            this.Get();
            s = new CharSet(); sR[0] = s;
            s.Fill();
        } else this.SynErr(46);
    }

    Char = (nR) => {
        this.Expect(Parser._char);
        nR[0] = 0;
        const tValU = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos));
        const subName = tValU.substr(1, tValU.length-2);
        const name = this.tab.Unescape(subName);

        // "<= 1" instead of "== 1" to allow the escape sequence '\0' in c++
        if (name.length <= 1) nR[0] = name[0].charCodeAt();
        else this.SemErr("unacceptable character value");
        if (this.dfa.ignoreCase && n >= 65 /* 'A' */ && n <= 90 /* 'Z' */) n ^= 32;
    }

    Sym = (nameR, kindR) => {
        nameR[0] = "???"; kindR[0] = this.id;
        const laKind = TokenRef.GetKind(this.laDv, this.lapos);
        if (laKind === Parser._ident) {
            this.Get();
            const tValU = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos));
            kindR[0] = this.id;
            nameR[0] = tValU;
        } else if (laKind === Parser._string || laKind === Parser._char) {
            if (laKind === Parser._string) {
                this.Get();
                const tValU = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos));
                nameR[0] = tValU;
            } else {
                this.Get();
                const tValU = util.UnicodeLoad(TokenRef.GetVal(this.tDv, this.tpos));
                const subName = tValU.substr(1, tValU.length-2);
                nameR[0] = "\"" + subName + "\"";
            }
            kindR[0] = this.str;
            if (this.dfa.ignoreCase) {
                nameR[0] = nameR[0].toLowerCase();
            }
            if (nameR[0].indexOf(' ') >= 0)
                this.SemErr("literal tokens must not contain blanks");
        } else this.SynErr(47);
    }

    Term = (gR, _g2R) => {
        const g2R = _g2R || [null];
        var rslv = null;
        gR[0] = null;
        if (this.StartOf(17)) {
            const laKind = TokenRef.GetKind(this.laDv, this.lapos);
            if (laKind === 37 /* IF */) {
                rslv = this.tab.NewNode(Node.rslv, null, TokenRef.GetLine(this.laDv, this.lapos));
                this.Resolver(rslv);
                gR[0] = new Graph(rslv);
            }
            this.Factor(g2R);
            if (rslv) this.tab.MakeSequence(gR[0], g2R[0]);
            else gR[0] = g2R[0];
            while (this.StartOf(18)) {
                this.Factor(g2R);
                this.tab.MakeSequence(gR, g2R);
            }
        } else if (this.StartOf(19)) {
            gR[0] = new Graph(this.tab.NewNode(Node.eps, null, 0));
        } else this.SynErr(48);
        if (!gR[0]) // invalid start of Term
            gR[0] = new Graph(this.tab.NewNode(Node.eps, null, 0));
    }

    Resolver = (node) => {
        this.Expect(37 /* "IF" */);
        this.Expect(30 /* "(" */);
        const beg = TokenRef.GetPos(this.laDv, this.lapos);
        const col = TokenRef.GetCol(this.laDv, this.lapos);
        const line = TokenRef.GetLine(this.laDv, this.lapos);
        this.Condition();
        node.pos = new Position(beg, TokenRef.GetPos(this.tDv, this.tpos), col, line);
    }

    Factor = (gR, _nameR, _kindR, _posR) => {
        const nameR = _nameR || [null];
        const kindR = _kindR || [null];
        var posR = _posR || { sempos: null };
        var weak = false;
        gR[0] = null;

        var laKind = TokenRef.GetKind(this.laDv, this.lapos);
        switch (laKind) {
        case Parser._ident: case Parser._string: case Parser._char: case 29 /* "WEAK" */:
            if (laKind === 29 /* "WEAK" */) {
                this.Get();
                weak = true;
            }
            this.Sym(nameR, kindR);
            const name = nameR[0];
            const kind = kindR[0];
            var sym = this.tab.FindSym(name);
            if (sym && kindR[0] === this.str)
                sym = tab.literals[name];
            var undef = !sym;
            if (undef) {
                if (kind === this.id)
                    sym = this.tab.NewSym(Node.nt, name, 0); // forward nt
                else if (this.genScanner) {
                    sym = this.tab.NewSym(Node.t, name, TokenRef.GetLine(this.tDv, this.tpos));
                    this.dfa.MatchLiteral(sym.name, sym);
                } else { // undefined string in production
                    this.SemErr("undefined string in production");
                    sym = this.tab.eofSy; // dummy
                }
            }
            var typ = sym.typ;
            if (typ != Node.t && typ != Node.nt)
                this.SemErr("this symbol kind is not allowed in a production");
            if (weak) {
                if (typ === Node.t) typ = Node.wt;
                else this.SemErr("only terminals may be weak");
            }
            const p = this.tab.NewNode(typ, sym, TokenRef.GetLine(this.tDv, this.tpos));
            gR[0] = new Graph(p);

            laKind = TokenRef.GetKind(this.laDv, this.lapos);
            if (laKind === 24 /* "<" */ || laKind === 26 /* "<." */) {
                this.Attribs(p);
                if (kind != this.id) this.SemErr("a literal must not have attributes");
            }
            if (undef)
                sym.attrPos = p.pos; // dummy
            else if (!p.pos !== !sym.attrPos)
                this.SemErr("attribute mismatch between declaration and use of this symbol");

            break;
        case 30 /* "(" */:
            this.Get();
            this.Expression(gR);
            this.Expect(31 /* ")" */);
            break;
        case 32 /* "[" */:
            this.Get();
            this.Expression(gR);
            this.Expect(33 /* "]" */);
            this.tab.MakeOption(gR[0]);
            break;
        case 34 /* "{" */:
            this.Get();
            this.Expression(gR);
            this.Expect(35 /* "}" */);
            this.tab.MakeIteration(gR[0]);
            break;
        case 39 /* "(." */:
            this.SemText(posR);
            const p = this.tab.NewNode(Node.sem, null, 0);
            p.pos = posR.sempos;
            gR[0] = new Graph(p);

            break;
        case 23 /* "ANY" */:
            this.Get();
            const p = this.tab.NewNode(Node.any, null, 0); // p.set is set in tab->SetupAnys
            gR[0] = new Graph(p);

            break;
        case 36 /* "SYNC" */:
            this.Get();
            const p = this.tab.NewNode(Node.sync, null, 0);
            gR[0] = new Graph(p);

            break;
        default: this.SynErr(49); break;
        }
        if (!gR[0]) // invalid start of Factor
            gR[0] = new Graph(this.tab.NewNode(Node.eps, null, 0));
    }

    Init = () => {
        this.tab = null;
        this.dfa = null;
        this.pgen = null;
        this.id = 0;
        this.str = 1;
        this.tokenString = null;
        this.noString = "-none-";
    }
}

Parser._EOF = 0;
Parser._ident = 1;
Parser._number = 2;
Parser._string = 3;
Parser._badString = 4;
Parser._char = 5;
Parser._ddtSym = 42;
Parser._optionSym = 43;

class Errors {
    constructor() {
        this.count = 0;
    }

    SynErr = (line, col, n) => {
        var s;
        switch (n) {
            case 0: s = "EOF expected"; break;
            case 1: s = "ident expected"; break;
            case 2: s = "number expected"; break;
            case 3: s = "string expected"; break;
            case 4: s = "badString expected"; break;
            case 5: s = "char expected"; break;
            case 6: s = "\"COMPILER\" expected"; break;
            case 7: s = "\"IGNORECASE\" expected"; break;
            case 8: s = "\"CHARACTERS\" expected"; break;
            case 9: s = "\"TOKENS\" expected"; break;
            case 10: s = "\"PRAGMAS\" expected"; break;
            case 11: s = "\"COMMENTS\" expected"; break;
            case 12: s = "\"FROM\" expected"; break;
            case 13: s = "\"TO\" expected"; break;
            case 14: s = "\"NESTED\" expected"; break;
            case 15: s = "\"IGNORE\" expected"; break;
            case 16: s = "\"PRODUCTIONS\" expected"; break;
            case 17: s = "\"=\" expected"; break;
            case 18: s = "\".\" expected"; break;
            case 19: s = "\"END\" expected"; break;
            case 20: s = "\"+\" expected"; break;
            case 21: s = "\"-\" expected"; break;
            case 22: s = "\"..\" expected"; break;
            case 23: s = "\"ANY\" expected"; break;
            case 24: s = "\"<\" expected"; break;
            case 25: s = "\">\" expected"; break;
            case 26: s = "\"<.\" expected"; break;
            case 27: s = "\".>\" expected"; break;
            case 28: s = "\"|\" expected"; break;
            case 29: s = "\"WEAK\" expected"; break;
            case 30: s = "\"(\" expected"; break;
            case 31: s = "\")\" expected"; break;
            case 32: s = "\"[\" expected"; break;
            case 33: s = "\"]\" expected"; break;
            case 34: s = "\"{\" expected"; break;
            case 35: s = "\"}\" expected"; break;
            case 36: s = "\"SYNC\" expected"; break;
            case 37: s = "\"IF\" expected"; break;
            case 38: s = "\"CONTEXT\" expected"; break;
            case 39: s = "\"(.\" expected"; break;
            case 40: s = "\".)\" expected"; break;
            case 41: s = "??? expected"; break;
            case 42: s = "this symbol not expected in Coco"; break;
            case 43: s = "this symbol not expected in TokenDecl"; break;
            case 44: s = "invalid TokenDecl"; break;
            case 45: s = "invalid AttrDecl"; break;
            case 46: s = "invalid SimSet"; break;
            case 47: s = "invalid Sym"; break;
            case 48: s = "invalid Term"; break;
            case 49: s = "invalid Factor"; break;
            case 50: s = "invalid Attribs"; break;
            case 51: s = "invalid TokenFactor"; break;

            default: s = `error ${n}`;
        }
        util.Print(`-- line ${line} col ${col}: ${s}`);
        this.count++;
    }

    Error = (line, col, s) => {
        util.Print(`-- line ${line} col ${col}: ${s}`);
        this.count++;
    }

    Warning = (line, col, s) => {
        if (col === undefined) {
            s = line;
            util.Print(s);
            return;
        }
        util.Print(`-- line ${line} col ${col}: ${s}`);
    }

    Exception = (s) => {
        util.Print(s);
        util.Exit(1);
    }
}
