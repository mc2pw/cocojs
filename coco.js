//See: http://2ality.com/2016/04/unhandled-rejections.html

import { Scanner } from '/scanner';
import { Parser } from '/parser';
import { Tab } as tab from '/tab';
import * as util from '/util';

function main(argc, argv) {
    util.print("Coco/R (Jan 02, 2012)");

    var srcName, nsName, frameDir, ddString, traceFileName;
    var outDir;
    var chTrFileName;
    var emitLines = false;

    for (var i = 1; i < argc; i++) {
        if (argv[i] === "-namespace" && i < argc - 1) nsName = argv[++i];
        else if (argv[i] === "-frames" && i < argc - 1) framesDir = argv[++i];
        else if (argv[i] === "-trace" && i < argc - 1) ddtString = argv[++i];
        else if (argv[i] === "-o" && i < argc - 1) outDir = argv[++i] + "/";
        else if (argv[i] === "-lines") emitLines = true;
        else srcName = argv[i]
    }

    if (argc > 0 && srcName) {
        var pos = srcName.lastIndexOf("/");
        if (pos < 0) pos = srcName.lastIndexOf("\\");
        var file = srcName;
        var srcDir = srcName.substr(0, pos+1);

        var scanner = new Scanner(file);
        var parser = new Parser(scanner);

        traceFileName = srcDir + "trace.txt";
        chTrFileName = traceFileName;

        (async () => {
            try {
                parser.trace = await util.fopenw(chTrFileName);
            } catch(e) {
                util.print(`-- could not open ${chTrFileName}`)
                util.exit(1);
            }

            parser.tab = new Tab(parser);
            parser.dfa = new DFA(parser);
            parser.pgen = new ParserGen(parser);

            parser.tab.srcName = srcName;
            parser.tab.srcDir = srcDir;
            parser.tab.nsName = nsName;
            parser.tab.frameDir = frameDir;
            parser.tab.outDir = outDir || srcDir;
            parser.tab.emitLines = emitLines;

            if (ddtString) parser.tab.SetDDT(ddtString);

            await parser.Parse();

            var fileSize = util.fsize(parser.trace);
            util.fclose(parser.trace);

            if (fileSize === 0) {
                util.remove(chTrFilename);
            } else {
                util.print(`trace output is in ${chTrFileName}`);
            }

            util.print(`${parser.errors.count} errors detected`);
            if (parser.errors.count != 0) {
                util.exit(1);
            }
        })();
    } else {
        util.print("Usage: Coco Grammar.ATG {Option}");
        util.print("Options:");
        util.print("  -namespace <namespaceName>");
        util.print("  -frames    <frameFilesDirectory>");
        util.print("  -trace     <traceString>");
        util.print("  -o         <outputDirectory>");
        util.print("  -lines");
        util.print("Valid characters in the trace string:");
        util.print("  A  trace automaton");
        util.print("  F  list first/follow sets");
        util.print("  G  print syntax graph");
        util.print("  I  trace computation of first sets");
        util.print("  J  list ANY and SYNC sets");
        util.print("  P  print statistics");
        util.print("  S  list symbol table");
        util.print("  X  list cross reference table");
        util.print("Scanner.frame and Parser.frame files needed in ATG directory");
        util.print("or in a directory specified in the -frames option.");
    }

    return 0;
}
